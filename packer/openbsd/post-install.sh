#!/bin/sh

set -e -v

cat >> /etc/sysctl.conf <<EOF
net.inet.ip.portfirst=32768
net.inet.ip.portlast=60999
EOF

echo "set timeout 1" >> /etc/boot.conf
sed -i "s/rw/rw,softdep/" /etc/fstab

pkg_add \
	bash \
	cmocka \
	gdb \
	git \
	gnutls \
	json-c \
	libmaxminddb \
	libuv \
	libxml \
	lmdb \
	nghttp2 \
	p5-IO-Socket-INET6 \
	p5-Net-DNS \
	py3-dnspython \
	py3-hypothesis \
	py3-pip \
	py3-ply \
	py3-requests \
	py3-test \
	sudo--

# Prefer GDB from ports
if [ -x /usr/bin/gdb ]; then
	mv /usr/bin/gdb /usr/bin/gdb.base
	mv /usr/local/bin/egdb /usr/bin/gdb
fi

# Apply all missing binary patches
if [ -n "$(syspatch -c)" ]; then
	syspatch
fi

echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

ftp -o /usr/local/bin/gitlab-runner http://users.isc.org/~michal/gitlab-runner
chmod +x /usr/local/bin/gitlab-runner

mkdir /builds
chmod 1777 /builds

# Fill all available disk space with a file containing zero bytes so that the
# parts of the QCOW2 image taken up by deleted files are replaced with zero
# bytes.  This significantly improves the compression ratio of the resulting
# QCOW2 image.
dd if=/dev/zero of=/EMPTY bs=1M || :
rm -f /EMPTY
sync
