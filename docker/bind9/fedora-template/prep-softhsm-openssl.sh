#!/bin/bash

set -e

mkdir -p /var/tmp/softhsm2/tokens
cat <<EOF > /var/tmp/softhsm2/softhsm2.conf
directories.tokendir = /var/tmp/softhsm2/tokens
objectstore.backend = file
log.level = DEBUG
EOF

mkdir -p /var/tmp/etc
sed "s|^openssl_conf = .*|openssl_conf = openssl_init|" /etc/ssl/openssl.cnf > /var/tmp/etc/openssl.cnf
cat <<EOF >>/var/tmp/etc/openssl.cnf

[openssl_init]
engines=engine_section

[engine_section]
pkcs11 = pkcs11_section

[pkcs11_section]
engine_id = pkcs11
dynamic_path = /usr/lib64/engines-1.1/pkcs11.so
MODULE_PATH = /usr/lib64/pkcs11/libsofthsm2.so
init = 0
EOF
