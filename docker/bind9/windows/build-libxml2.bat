CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64

C:\tools\cygwin\bin\sh.exe -l -c "curl ftp://xmlsoft.org/libxml2/%1.tar.gz | tar --directory /cygdrive/c --extract --gzip"

CD "%1\win32"
cscript.exe configure.js iconv=no
nmake.exe -f Makefile.msvc
CD ..
xcopy.exe /E include C:\libxml2\include\
MKDIR C:\libxml2\win32\bin.msvc
COPY win32\bin.msvc\libxml2* C:\libxml2\win32\bin.msvc\
CD ..
RMDIR /S /Q "%1"
