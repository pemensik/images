CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64

powershell.exe -Command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile('https://github.com/nghttp2/nghttp2/archive/v%1.zip', '%1.zip')"
powershell.exe -Command "Expand-Archive -Path %1.zip -DestinationPath ."
DEL %1.zip

CD "nghttp2-%1"
MKDIR build
CD build
"C:\Program Files\cmake\bin\cmake.exe" -DCMAKE_GENERATOR_PLATFORM=x64 -DCMAKE_INSTALL_PREFIX=C:\nghttp2 ..
"C:\Program Files\CMake\bin\cmake.exe" --build . --config Release
"C:\Program Files\cmake\bin\cmake.exe" -P cmake_install.cmake
CD ..\..
RMDIR /S /Q "nghttp2-%1"
