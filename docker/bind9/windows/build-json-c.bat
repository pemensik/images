CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64

powershell.exe -Command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile('https://github.com/json-c/json-c/archive/%1.zip', '%1.zip')"
powershell.exe -Command "Expand-Archive -Path %1.zip -DestinationPath ."
DEL %1.zip

CD "json-c-%1"
MKDIR build
CD build
"C:\Program Files\cmake\bin\cmake.exe" -DCMAKE_GENERATOR_PLATFORM=x64 -DCMAKE_INSTALL_PREFIX=C:\json-c ..
msbuild.exe /p:Configuration=Release json-c.vcxproj
"C:\Program Files\cmake\bin\cmake.exe" -P cmake_install.cmake
CD ..\..
RMDIR /S /Q "json-c-%1"
