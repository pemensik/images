powershell.exe -Command "[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12; (New-Object System.Net.WebClient).DownloadFile('https://github.com/libuv/libuv/archive/v%1.zip', '%1.zip')"
powershell.exe -Command "Expand-Archive -Path %1.zip -DestinationPath ."
DEL %1.zip

CD "libuv-%1"
C:\tools\cygwin\bin\sed.exe -i "s/^\(int uv__tcp_xfer_\)/UV_EXTERN \1/" src/win/internal.h
MKDIR build
CD build
"C:\Program Files\CMake\bin\cmake.exe" -DCMAKE_GENERATOR_PLATFORM=x64 -DBUILD_TESTING=OFF ..
"C:\Program Files\CMake\bin\cmake.exe" --build . --config Release
CD ..
MKDIR C:\libuv\build\Release
xcopy.exe /E include C:\libuv\include\
COPY build\Release\uv.dll C:\libuv\build\Release\
COPY build\Release\uv.lib C:\libuv\build\Release\
CD ..
RMDIR /S /Q "libuv-%1"
