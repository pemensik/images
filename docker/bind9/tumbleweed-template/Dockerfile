FROM opensuse/@SOURCE_IMAGE@
MAINTAINER BIND 9 Developers <bind9-dev@isc.org>
ENV ATF_VERSION 0.21
ENV LUTOK_VERSION 0.4
ENV KYUA_GIT_COMMIT @KYUA_GIT_COMMIT@
RUN zypper -n dup
RUN zypper -n install \
	ccache \
	curl \
	diffutils \
	findutils \
	gcc \
	gcc-c++ \
	gdb \
	git \
	gnutls \
	iproute2 \
	jemalloc-devel \
	krb5-devel \
	libcap-devel \
	libcmocka-devel \
	libidn2-devel \
	libjson-c-devel \
	libmaxminddb-devel \
	libnghttp2-devel \
	libopenssl-devel \
	libtool \
	libuv-devel \
	libxml2-devel \
	lmdb-devel \
	lua-devel \
	make \
	openldap2-devel \
	perl \
	perl-IO-Socket-INET6 \
	perl-JSON \
	perl-Net-DNS \
	perl-XML-Simple \
	procps \
	python3-dnspython \
	python3-hypothesis \
	python3-pip \
	python3-ply \
	python3-pytest \
	python3-requests \
	sqlite3-devel \
	system-user-nobody \
	timezone
RUN if [ "@DNSTAP@" = "yes" ]; then zypper -n install fstrm-devel protobuf-c; fi
COPY kyua.tar /
RUN if [ ! -s /kyua.tar ]; then \
	curl -sSL "https://github.com/jmmv/atf/releases/download/atf-${ATF_VERSION}/atf-${ATF_VERSION}.tar.gz" | tar -xz -C /usr/src && \
	cd "/usr/src/atf-${ATF_VERSION}" && \
	./configure --prefix /usr --libdir /usr/lib64 && \
	make -j@BUILD_PARALLEL_JOBS@ && \
	make install && \
	make install DESTDIR=/tmp/kyua-cache-root && \
	cd .. && rm -rf /tmp/kyua-cache-root/usr/tests "/usr/src/atf-${ATF_VERSION}" && \
	ldconfig && \
	curl -sSL "https://github.com/jmmv/lutok/releases/download/lutok-${LUTOK_VERSION}/lutok-${LUTOK_VERSION}.tar.gz" | tar -xz -C /usr/src && \
	cd "/usr/src/lutok-${LUTOK_VERSION}" && \
	./configure --prefix /usr --libdir /usr/lib64 && \
	make -j@BUILD_PARALLEL_JOBS@ && \
	make install && \
	make install DESTDIR=/tmp/kyua-cache-root && \
	cd .. && rm -rf /tmp/kyua-cache-root/usr/tests "/usr/src/lutok-${LUTOK_VERSION}" && \
	ldconfig && \
	curl -sSL "https://github.com/Mno-hime/kyua/archive/${KYUA_GIT_COMMIT}.tar.gz" | tar -xz -C /usr/src && \
	cd "/usr/src/kyua-${KYUA_GIT_COMMIT}" && \
	./configure --prefix /usr --libdir /usr/lib64 && \
	make -j@BUILD_PARALLEL_JOBS@ && \
	make install && \
	make install DESTDIR=/tmp/kyua-cache-root && \
	cd .. && rm -rf /tmp/kyua-cache-root/usr/tests "/usr/src/kyua-${KYUA_GIT_COMMIT}" && \
	tar --directory /tmp/kyua-cache-root --create --file /kyua.tar . ; \
    fi
RUN tar --extract --file /kyua.tar
RUN ldconfig
